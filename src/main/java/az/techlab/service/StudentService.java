package az.techlab.service;

import az.techlab.entity.Student;
import az.techlab.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public List<Student> getList(int page, int limit) {
        Pageable pageable = PageRequest.of(page, limit);
        Page<Student> students = studentRepository.findAll(pageable);
        return students.getContent();
    }

    public Student get(Integer id) {
        return studentRepository.findById(id).orElseThrow(() -> new RuntimeException("Student with id:" + id + " not found"));
    }

    public Student create(Student student) {
        return studentRepository.save(student);
    }

    public Student update(Integer id, Student student) {
        this.get(id);
        student.setId(id);
        return  studentRepository.save(student);
    }

    public void delete(Integer id) {
        studentRepository.deleteById(id);
    }
}
