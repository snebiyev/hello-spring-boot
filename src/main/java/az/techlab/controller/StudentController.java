package az.techlab.controller;

import az.techlab.entity.Student;
import az.techlab.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Student student) {
        return ResponseEntity.status(HttpStatus.CREATED).body(studentService.create(student));
    }

    @GetMapping
    public ResponseEntity<List<?>> getList(@RequestParam(value = "page", defaultValue = "0") int page,
                                 @RequestParam(value = "limit", defaultValue = "20") int limit) {
        if (page > 0) page -= 1;
        return ResponseEntity.status(HttpStatus.CREATED).body(studentService.getList(page, limit));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable int id) {
        Student student = studentService.get(id);
        return ResponseEntity.ok().body(student);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable  Integer id, @RequestBody Student student) {
        return ResponseEntity.ok().body(studentService.update(id, student));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Integer id) {
        studentService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
